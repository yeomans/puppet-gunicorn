# Puppet-gunicorn

Puppet-gunicorn is a Puppet module that installs a Python 3 virtual environment with 
gunicorn and your python wsgi application.

It is currently purpose-built for my needs, and so written for Debian Jessie with 
systemd used to manage gunicorn.  The Puppet code is written for Puppet 4, but may work with
recent versions of Puppet 3.


# class gunicorn

This is the main class for the module.  You should be able 

## Parameters:

`String $venv_path`: The path at which to create the virtual environment. 
`Optional[String] $user=undef`: Owner of virtual environment directory, and the gunicorn processes.  
If undef, then a system user with name `basename $venv_path` is created with shell /bin/false. 
If you supply a user, then it's up to you to create it.
 
`Optional[String] $group=undef`: Group of virtual environment directory, and the gunicorn 
processes.

`$app_module`: The app module expected by gunicorn.
`$config='gunicorn-conf.py'`: The gunicorn configuration.  Pass a reference to a python 
module, or don't pass anything and instead supply arguments to gunicorn::config.
