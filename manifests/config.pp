class gunicorn::config
    (
    # config file settings
    String $config_path,
    String $owner, 
    String $group,
    String $settings
    )
    {
    file
        {
        "$config_path":
        owner => "$owner",
        group => "$group",
        mode => '0644',
        content => "$settings",
        notify => Class['gunicorn::service'],
        }
    }