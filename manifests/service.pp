# This class is written for systemd only.

class gunicorn::service
    (
    Enum['enabled', 'disabled', 'started', 'stopped', 'absent'] $ensure='started',
    $exc_cmd,
    $user='root', 
    $group='root',
    Array $bind=['127.0.0.1:8000'],
    $working_dir,
    $run_dir='/run/gunicorn',
    $pid='/run/gunicorn/pid',
    $config, 
    $app_module,
    Hash $environment={},
    )
    {
    $file_ensure = $ensure ? {'absent' => 'absent', default => 'file'}
    
    file
        {
        '/etc/systemd/system/gunicorn.service':
        ensure => "$file_ensure", 
        owner => 'root',
        group => 'root',
        mode => '0644', 
        content => template('gunicorn/gunicorn.service.erb'),
        notify => Exec["gunicorn_daemon_reload"],
        }

    file
        {
        '/etc/systemd/system/gunicorn.socket':
        ensure => "$file_ensure", 
        owner => 'root',
        group => 'root',
        mode => '0644', 
        content => template('gunicorn/gunicorn.socket.erb'),
        notify => Exec["gunicorn_daemon_reload"],        }  

    exec
        {
        'gunicorn_daemon_reload':
        command =>'systemctl daemon-reload',
        path => "$path",
        refreshonly => true,
        }
        
    if $ensure == 'enabled' or $ensure == 'started'
        {        
        exec
            {
            "gunicorn_enable":
            command =>'systemctl enable gunicorn.socket',
            path => "$path",
            unless => "systemctl is-enabled gunicorn.socket"
            }
        if $ensure == 'started'
            {
             exec
                {
                "gunicorn_start":
                command =>'systemctl start gunicorn.socket',
                path => "$path",
                unless => "systemctl is-active gunicorn.socket"
                }
            }
        }
    else
        {
         exec
            {
            "gunicorn_stop":
            command =>'systemctl stop gunicorn.socket',
            path => "$path",
            onlyif => "systemctl is-active gunicorn.socket"
            }
        if $ensure == 'disabled' or $ensure == 'absent'
            {  
            exec
                {
                "gunicorn_disable":
                command =>'systemctl disable gunicorn.socket',
                path => "$path",
                onlyif => "systemctl is-enabled gunicorn.socket"
                }
            }
        }
    }